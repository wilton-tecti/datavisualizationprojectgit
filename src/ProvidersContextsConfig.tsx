import React from "react";
import {SWRConfig} from "swr";
import { Theme, ThemeProvider } from 'theme-ui';
import api from "./core/services/api";
import ThemeFactory from './core/theme/factory';

interface Props{
}
const ProvidersContextsConfig : React.FC<Props> = ({children}) => {
  const theme: Theme = ThemeFactory.create();
  return (
    <SWRConfig value={{fetcher: async (url: string )  => {const res = await api.get(url); return res.data}}}>
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    </SWRConfig>

  );
}

export default ProvidersContextsConfig;