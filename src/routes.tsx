import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

  import Projects from "./projects/routes";
  import Login from "./user/pages/login";

export default function Routes(){

    return (
        <Router>
            <Switch >  
                <Route exact path="/" component={Login} />
                <Route path="/projects" component={Projects} />
            </Switch>
         </Router>
    )
}