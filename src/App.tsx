import React from "react";

import ProvidersContextsConfig from "./ProvidersContextsConfig";
import Routes from "./routes";
interface Props{
}
const App : React.FC<Props> = () => {

  return (
   <ProvidersContextsConfig>
     <Routes/>
    </ProvidersContextsConfig>

  );
}

export default App;