import React, { ChangeEvent, useState } from "react";
import { useHistory } from "react-router-dom";
import { Box, Button, Container, Flex, Heading, Input, Label, Message } from "theme-ui";
import { login } from "../../../core/services/auth";


const Login  = () => {
    const [token, setToken] = useState("");
    const history = useHistory()

    function handleSubmit(){
        if(token.length > 0 ){
            login(token)
           history.push("/projects")
        }
    }

    return(
        <Flex
            sx={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                bg:"secondary",
                height:"100vh",
                width: "100vw"
            }}
        >
            <Container
                sx={{
                    maxWidth: "1000px",
                    width: "100%",
                }}
            >
                <Box
                    color="white"
                >
                    <Heading as="h1" sx={{textAlign:"center"}}>
                        Sistema de métricas ágeis
                    </Heading>
                    <Heading as="h2">
                        integrado com GitLab
                    </Heading>
                    <Label>Token</Label>
                    <Input name="token" id="token" type="text" bg="white" color="primary" value={token} onChange={ (e: ChangeEvent<HTMLInputElement>)=> setToken(e.target.value)}/>
                    <Message variant="white">
                        Insira aqui o seu token do Gitlab 
                    </Message>
                    <Button onClick={ () => handleSubmit() }>Acessar Sistema de métricas</Button>
                </Box>
            </Container>
        </Flex>
    )

}

export default Login