import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

import Login from "./pages/login";

export default function Routes(){

    return (
        <Router>
            <Switch >  
                <Route path="/" component={Login} />
            </Switch>
         </Router>
    )
}