import axios from "axios";
import {baseURL} from '../../config/url'
import {getToken} from '../../services/auth'
let CancelToken = axios.CancelToken;
export let cancelToken = CancelToken.source();


const api = axios.create({
  baseURL,
});

api.interceptors.request.use( config => {

    const token = getToken();
  
   if (token) {
     config.headers.Authorization = `Bearer ${token}`;
   }

   
   return config;
});

api.interceptors.response.use( response => {  
     return response
});



export default api;
