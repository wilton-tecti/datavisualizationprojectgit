import React from "react";
import { useParams } from "react-router-dom";
import { Badge, Button, Card, Flex, Image, Text } from "theme-ui";
import Locale from "../../../core/utils/Date/Locale";
import useProjectDetail from "../../hooks/useProjectDetail";
import useProjectDetailIssues from "../../hooks/useProjectDetailIssues";



function List(){
    let { id } = useParams();
    const {data: project}= useProjectDetail(id);
    const {data: issues}= useProjectDetailIssues(id);

    if(!project) return <p>Carregando...</p>
    return(
        <Flex sx={{
            flexDirection:"column",
        }}>

            <Card key={project.id} >
                
                <Text>{project.id}</Text>
                <Text>{project.name}</Text>

                {project.tag_list?.map( (tag: string)  =>  <Badge>{tag}</Badge>)}
                
            </Card>

            {issues && Object.values(issues).map( issue => 
            <Card key={issue?.id} >
                
                <Badge>{issue.id}</Badge>
                <Badge>{issue.title}</Badge>
                <Badge>{new Date(issue.created_at).toLocaleDateString(Locale.PTBR())}</Badge>
                <Badge>{issue.closed_at?.toLocaleDateString(Locale.PTBR())}</Badge>

                                    
            </Card>
                )}


        </Flex>
        )

}

export default List;