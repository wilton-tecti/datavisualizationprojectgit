import React from "react";
import { useHistory } from "react-router-dom";
import { Button, Card, Flex, Image, Text } from "theme-ui";
import Project from "../../entity/Project";
import useProjetcs from "../../hooks/useProjects";


function List(){
    const history = useHistory();
    const {data: projects} = useProjetcs<Project>();

    if(!projects) return <p>Carregando...</p>
    return(
        <div>

        {projects.map( (project) => {
            return(
                <Flex sx={{
                    flexDirection:"column",
                }}>

                    <Card key={project.id} >
                        
                        <Text>{project.id}</Text>
                        <Text>{project.name}</Text>
                        <Button onClick={ ()=> history.push(`projects/${project.id}`)}>{project.name}</Button>
                    </Card>
                </Flex>
                )
            })}
    </div>
    )
}

export default List;