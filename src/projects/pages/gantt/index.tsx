import { randomBytes } from "crypto";
import React from "react";
import Chart from "react-google-charts";
import { useParams } from "react-router-dom";
import Issue from "../../entity/Issue";
import useProjectDetailIssues from "../../hooks/useProjectDetailIssues";

interface struct {
    type: string;
    label: string;
}
function mockDateFinal (date: any){
    const random = Math.random() * 10 ;
    console.log(random)
    const newDate = new Date(date)
    newDate.setDate(random < 30 ? Number.parseInt(random.toPrecision(1)) : 29 );
    return newDate; 

}


function Gantt(){
    let { id } = useParams();
    const {data: issues}= useProjectDetailIssues(id);
    function prepareDataToGrafic(issues: Issue[]){
        const struct : struct[] =  [
            { type: 'string', label: 'Task ID' },
            { type: 'string', label: 'Task Name' },
            { type: 'date', label: 'Start Date' },
            { type: 'date', label: 'End Date' },
            { type: 'number', label: 'Duration' },
            { type: 'number', label: 'Percent Complete' },
            { type: 'string', label: 'Dependencies' },
        ];
       
        const rows = issues.map( (issue: Issue, i: number) =>{ return [issue.id, issue.title, new Date(issue.created_at), new Date(2021, i+1, (i+ 1) * 2), null, 100, null]});
        
        console.log([struct, ...rows])
        return [struct, ...rows];

    }
    if(!issues) return <p>Carregando...</p>
    return(
        <Chart
        width={'100%'}
        height={'400px'}
        chartType="Gantt"
        loader={<div>Loading Chart</div>}
        data={prepareDataToGrafic(issues)}
        rootProps={{ 'data-testid': '1' }}
        />
    )
}

export default Gantt