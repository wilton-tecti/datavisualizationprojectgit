import React from "react";
import useSWR from "swr";
import Project from "../../entity/Project";

function useProjectDetail(id:number){

    const { data, error } =  useSWR<Project>(`/projects/${id}`)

 
    return {data,error};

}

export default useProjectDetail;