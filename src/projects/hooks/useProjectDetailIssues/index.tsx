import React from "react";
import useSWR from "swr";
import Issue from "../../entity/Issue";
import Project from "../../entity/Project";

function useProjectDetailIssues(id:number){

    const { data, error } =  useSWR<Issue[]>(`/projects/${id}/issues`)

 
    return {data,error};

}

export default useProjectDetailIssues;