import React from "react";
import useSWR from "swr";
import Project from "../../entity/Project";

function useProjetcs<T>(){
    
    const {data, error} = useSWR<T[]>("projects?owned=true");

    return {data, error}

}

export default useProjetcs;