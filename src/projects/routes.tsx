import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

  import List from "./pages/list";
  import Detail from "./pages/detail";
  import Gantt from "./pages/gantt";

export default function Routes(){

    return (
        <Router>
            <Switch >  
                <Route exact path="/projects" component={List} />
                <Route exact path="/projects/:id" component={Detail} />
                <Route exact path="/projects/:id/Gantt" component={Gantt} />
            </Switch>
         </Router>
    )
}