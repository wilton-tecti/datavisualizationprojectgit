export default interface Project{
    id: number;
    name: string;
    tag_list?: string[];
    _links?: _links;
    
}


interface _links {
    "self": string;
    "issues": string;
    "merge_requests": string;
    "repo_branches": string;
    "labels": string;
    "events": string;
    "members": string;
}