export default interface Issue {
    "state" : string;
    "title" : string;
    "author" : string;
    "id" : string;
    "web_url" : string;
    "name" : string;
    "avatar_url" : string;
    "username" : string;
    "milestone" : string;
    "created_at": Date;
    "closed_at": Date;
}